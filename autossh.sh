#!/bin/bash
lokasi="/var/log/o2o/ssh/"
exec 3>&1 4>&2
trap 'exec 2>&4 1>&3' 0 1 2 3
exec 1>"$lokasi""$(date +"%d-%m-%Y_%I:%M")_o2o.log" 2>&1
startlap_timestamp=$(date +%s)


if lsof -Pi :202 -sTCP:ESTABLISHED -t >/dev/null ; then
    date
    echo "running"
else
    date
    /usr/lib/autossh/autossh -M 2000 -p 202 -f -N -R 2222:localhost:22 oduao@103.107.xxx.xxx -C
    echo "connecting to ssh server"
fi